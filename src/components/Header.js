import PropTypes from 'prop-types'
import {useLocation} from 'react-router-dom'
import Button from './Button'

const Header = ({ title, onAdd, showAdd }) => {
    {/*Tambien se puede colocar ({title})*/}
    const location = useLocation()
    
    return (
        <header className='header'>
            <h1>{title}</h1>
            { location.pathname === '/' && 
                <Button color={!showAdd ? 'green' : 'red'} text={!showAdd ? 'Add' : 'Close'} onClick={onAdd} />
            }
            {/*
            style={headingStyle}
            <h1>{props.title}</h1>
            <h1>Task Tracker</h1>
            */}
        </header>
    )
}

Header.defaultProps = {
    title: 'Task Tracker'
}

Header.prototypes = {
    title: PropTypes.string.isRequired
}

const headingStyle = {
    color: 'red',
    backgroundColor: 'black'
}

export default Header
