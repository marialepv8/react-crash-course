import Task from './Task'

const Tasks = ({tasks, onDelete, onToggle}) => {

{/*
    Aqui estaba el arreglo del task con el useState, este se cambio a App
    const [tasks, setTasks] = useState([
        {
            id: 1,
            text: 'Doctors Appointment',
            day: 'Feb 5th at 2:30pm',
            reminder: true
        },
        {
            id: 2,
            text: 'Meeting at School',
            day: 'Feb 6th at 1:30pm',
            reminder: true
        },
        {
            id: 3,
            text: 'Food Shopping',
            day: 'Feb 5th at 2:30pm',
            reminder: false
        }
    ]) para el key tambien se puede usar index en vez de task.id
*/}
    return (
        <>
            {
                tasks.map((task) => (
                    <Task key={task.id} task={task} onDelete={onDelete} onToggle={onToggle}/>
                )) 
            }
        </>
    )
}

export default Tasks
